import hashlib
import jdatetime
import random
import uuid
import json
from .mapsearch import *
from zeep import Client as zarinpalClient
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.db.models import Q
from .models import UsersTemp, Users, Tokens, CoffeeShopPictues, CoffeeShops, CoffeeShopsMenus, CoffeeShopsWrokTime
from .models import CoffeeShopsDiscounts, OrdersToPay, OrdersPayments, Messages
from math import sin, cos, sqrt, atan2, radians

path = ""
caffeMerchantCode = ""
caffePaymentCallbackURL = str(path) + "resultTeamPayment/"


def md5_one(Pss):
    s = str(Pss)
    res = hashlib.md5(s.encode())
    return str(res.hexdigest())
def md5(Pss):
    s = str(Pss)
    for i in range(0, 10):
        s = md5_one(s)
    return str(s)
def res_generator(status_code: int, message: str):
    s = {'status': status_code,
         'data': {'message': message}
         }
    res = Response(s)
    return res
def token_res_generator(token):
    s = {'status': 200,
         'data': {'api_token': str(token)}
         }
    res = Response(s)
    return res
def token_generator():
    return str(uuid.uuid4().hex[:32])
def send_confirm_code_with_sms(confirm_code: int):
    pass
def send_new_pass_with_sms(new_pass: str):
    pass
def error_generator(status: int, message: str):
    s = {'message': message}
    res = Response(s, status=status, )
    return res
def confirm_code_generator():
    return random.randint(1001, 9999)
def getNowDate():
    return str(jdatetime.datetime.now().strftime("%y/%m/%d"))
def getNowTime():
    return str(jdatetime.datetime.now().strftime("%H:%M:%S"))
def getUserIdFromApiToken(api_token: str):
    tks = Tokens.objects.filter(api_token=api_token)
    if tks.__len__() == 0:
        return None
    else:
        return int(tks[0].user_id)
def saveNewTokenToDb(user_id: int, agent: str, fcm_token: str):
    tokens = Tokens.objects.filter(user_id=user_id).order_by('-token_id')
    if tokens.__len__() == 0:
        api_token = token_generator()
        token = Tokens()
        token.api_token = str(api_token)
        token.user_id = int(user_id)
        token.agent = str(agent)
        token.fcm_token = str(fcm_token)
        token.save()
        return str(api_token)
    token = tokens[0]
    Tokens.objects.filter(token_id=token.token_id).update(fcm_token=fcm_token)
    return str(token.api_token)
def isApiTokenValid(api_token: str):
    ts = Tokens.objects.filter(api_token=api_token)
    return ts.__len__() != 0
def replaceText(oldChar: str, newChar: str, text: str):
    try:
        return text.replace(oldChar, newChar)
    except Exception as e:
        return text
def getPersianTextInStandardFormat(text: str):
    text = text.lower()
    text = replaceText('ض', 'ز', text)
    text = replaceText('ص', 'س', text)
    text = replaceText('ش', 'س', text)
    text = replaceText('ظ', 'ز', text)
    text = replaceText('ث', 'س', text)
    text = replaceText('ج', 'ح', text)
    text = replaceText('خ', 'ح', text)
    text = replaceText('ہ', 'ه', text)
    text = replaceText('ي', 'ی', text)
    text = replaceText('ے', 'ی', text)
    text = replaceText('ڪ', 'ک', text)
    text = replaceText('ذ', 'ز', text)
    text = replaceText('ؤ', 'ی', text)
    text = replaceText('ء', 'ی', text)
    text = replaceText(".", " ", text)
    return text
def searchInCoffeeShops(search_text: str):
    coffee_shops = CoffeeShops.objects.all()
    result = []
    for shop in coffee_shops:
        if not getPersianTextInStandardFormat(str(shop.name)).__contains__(getPersianTextInStandardFormat(str(search_text))):
            continue
        result.append(shop)
    return result
def getDistanceBetweenTwoPoints(lat1: float, lng1: float, lat2: float, lng2: float):
    R = 6373.0
    lat1_rad = radians(lat1)
    lng1_rad = radians(lng1)
    lat2_rad = radians(lat2)
    lng2_rad = radians(lng2)

    dlng = lng2_rad - lng1_rad
    dlat = lat2_rad - lat1_rad

    a = sin(dlat / 2) ** 2 + cos(lat1_rad) * cos(lat2_rad) * sin(dlng / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c  # kiloometers
    return abs(float(distance*1000))
def searchNearbyCoffeeShops(lat: float, lng: float):
    result = []
    shops = CoffeeShops.objects.all()
    for shop in shops:
        shop_lat = float(shop.lat)
        shop_lng = float(shop.lng)
        if getDistanceBetweenTwoPoints(lat, lng, shop_lat, shop_lng) > 1000:
            continue
        result.append(shop)

    return result
def getMainPictureOfShop(shop_id: int):
    pics = CoffeeShopPictues.objects.filter(shop_id=shop_id, isMain=True).order_by('-picture_id')
    if pics.__len__() != 0:
        return str(pics[0].link)
    else:
        return ""
def getPicturesOfShop(shop_id: int):
    pics = CoffeeShopPictues.objects.filter(shop_id=shop_id, isMain=False).order_by('-picture_id')
    res = []
    for pic in pics:
        res.append(dict(link=str(pic.link)))
    return res
def getShopWorkTime(shop_id: int):
    times = CoffeeShopsWrokTime.objects.filter(shop_id=shop_id).order_by('time_id')
    if times.__len__() == 0:
        return ""
    res = ""
    for time in times:
        res += str(time.start) + " تا " + str(time.end)
        res += "\n"
    return res.strip()
def getShopMenu(shop_id: int):
    items = CoffeeShopsMenus.objects.filter(shop_id=shop_id, exist=True).order_by('item_id')
    res = []
    for item in items:
        res.append(dict(item_id=int(item.item_id),
                        name=str(item.name),
                        price=int(item.price),
                        picture_link=str(item.picture_link),
                        item_type=str(item.item_type)))
    return res
def getOrdersListFromJsonString(jStr: str):
    res = []
    items = json.loads(jStr)
    for item in items:
        res.append(int(item['item_id']))
    return res
def getDiscountPrecentFromCode(discount_code: str):
    discounts = CoffeeShopsDiscounts.objects.filter(code=discount_code).order_by('-discount_id')
    if discounts.__len__() == 0:
        return 0
    return int(discounts[0].precent)
def getShopNameFromShopId(shop_id: int):
    shops = CoffeeShops.objects.filter(shop_id=shop_id)
    if shops.__len__() == 0:
        return ""
    return str(shops[0].name)
def decreaseDiscountUseCountFromCode(discount_code: str):
    discounts = CoffeeShopsDiscounts.objects.filter(code=discount_code).order_by('-discount_id')
    if discounts.__len__() == 0:
        return
    if int(discounts[0].use_count) == 1:
        CoffeeShopsDiscounts.objects.filter(code=discount_code).delete()
    else:
        CoffeeShopsDiscounts.objects.filter(code=discount_code).update(int(discounts[0].use_count)-1)
def getOrdersCountFromOrdersList(orders_list: str):
    try:
        arr = json.loads(orders_list)
        return int(arr.__len__())
    except:
        return 0
def getOrderDateAndTimeWithOrderId(order_id: int):
    payments = OrdersPayments.objects.filter(order_id=order_id).order_by('-payment_id')
    if payments.__len__() == 0:
        return "", ""
    return str(payments[0].date), str(payments[0].time)
def dateToDay(year,month,day):
    day+=year*365
    if month<=6:
        day+=month*31
    else:
        day+=month*30
    return day
def isMessagesDateOk(date: str):
    strDate = str(date).strip()
    split = strDate.split("/")
    year = int(split[0])
    month = int(split[1])
    day = int(split[2])
    nowYear = int(jdatetime.datetime.now().strftime("%y"))
    nowMonth = int(jdatetime.datetime.now().strftime("%m"))
    nowDay = int(jdatetime.datetime.now().strftime("%d"))

    if abs(dateToDay(nowYear, nowMonth, nowDay) - dateToDay(year, month, day)) <= 31:
        return True
    else:
        return False


@csrf_exempt
@api_view(['POST'])
def signup(request):
    if request.method != 'POST':
        return res_generator(405, "methode_not_allowed")

    username = request.POST['username']
    Pass = request.POST['pass']
    phoneNumber = request.POST['phone_number']
    agent = request.POST['agent']
    fcm_token = request.POST['fcm_token']

    try:
        confirm_code = confirm_code_generator()

        user_temp = UsersTemp()
        user_temp.username = str(username)
        user_temp.pass_field = str(Pass)
        user_temp.phone_number = str(phoneNumber)
        user_temp.agent = str(agent)
        user_temp.fcm_token = str(fcm_token)
        user_temp.confirm_code = int(confirm_code)

        checkUsername = Users.objects.filter(username=username)
        if checkUsername.__len__() != 0:
            return res_generator(409, "duplicate_username")

        checkPhoneNumber = Users.objects.filter(phone_number=phoneNumber)
        if checkPhoneNumber.__len__() != 0:
            return res_generator(409, "duplicate_phone_number")

        user_temp.save()
        send_confirm_code_with_sms(confirm_code)
        return res_generator(200, "ok")

    except Exception as e:
        return error_generator(400, str(e))


@csrf_exempt
@api_view(['POST'])
def login(request):
    if request.method != 'POST':
        return res_generator(405, "methode_not_allowed")

    username = request.POST['username']
    Pass = request.POST['pass']
    agent = request.POST['agent']
    fcm_token = request.POST['fcm_token']

    users_list = Users.objects.filter(username=username).order_by('-user_id')
    if users_list.__len__() == 0:
        return res_generator(404, "username_or_pass")
    user = users_list[0]

    if user.with_google != 0:
        return res_generator(405, "not_this_way")

    if user.pass_field != Pass and user.pass_field != md5(Pass):
        return res_generator(404, "username_or_pass")

    api_token = saveNewTokenToDb(int(users_list[0].user_id), str(agent), str(fcm_token))
    return token_res_generator(api_token)


@csrf_exempt
@api_view(['POST'])
def confirmCode(request):
    if request.method != 'POST':
        return res_generator(405, "methode_not_allowed")

    confirm_code = request.POST['confirm_code']
    username = request.POST['username']
    date = getNowDate()

    try:
        usersTemp = UsersTemp.objects.filter(confirm_code=int(confirm_code), username=username).order_by('-user_id')
        if usersTemp.__len__() == 0:
            return res_generator(401, "confirm_code")

        userTemp = usersTemp[0]

        user = Users(username=userTemp.username,
                     pass_field=userTemp.pass_field,
                     phone_number=userTemp.phone_number,
                     join_date=date)
        user.save()

        api_token = saveNewTokenToDb(int(user.user_id), str(userTemp.agent), str(userTemp.fcm_token))
        return token_res_generator(str(api_token))
    except Exception as e:
        return error_generator(400, str(e))


@csrf_exempt
@api_view(['POST'])
def forgetPass(request):
    if request.method != 'POST':
        return res_generator(405, "methode_not_allowed")

    usernameOrPhoneNumber = request.POST['username_or_phone_number']

    try:

        with_username = True

        users = Users.objects.filter(username=usernameOrPhoneNumber).order_by("-user_id")
        if users.__len__() == 0:
            with_username = False
            users = Users.objects.filter(phone_number=usernameOrPhoneNumber).order_by("-user_id")
            if users.__len__() == 0:
                return res_generator(404, "not_found")

        new_pass = token_generator()[:8]
        if with_username:
            Users.objects.filter(username=usernameOrPhoneNumber).update(pass_field=md5(new_pass))
        else:
            Users.objects.filter(phone_number=usernameOrPhoneNumber).update(pass_field=md5(new_pass))

        send_new_pass_with_sms(new_pass)
        return res_generator(200, "ok")

    except Exception as e:
        return error_generator(400, str(e))


@csrf_exempt
@api_view(['POST'])
def loginWithGoogle(request):
    if request.method != 'POST':
        return res_generator(405, "methode_not_allowed")

    username = request.POST['username']
    email = request.POST['email']
    agent = request.POST['agent']
    fcm_token = request.POST['fcm_token']

    users_list = Users.objects.filter(email=email).order_by('-user_id')
    if users_list.__len__() == 0:
        user = Users()
        user.username = str(username)
        user.email = str(email)
        user.join_date = getNowDate()
        user.with_google = 1
        user.save()
        api_token = saveNewTokenToDb(int(user.user_id), str(agent), str(fcm_token))
        return token_res_generator(api_token)
    user = users_list[0]

    if user.with_google == 1:
        api_token = saveNewTokenToDb(int(user.user_id), str(agent), str(fcm_token))
        return token_res_generator(api_token)

    return res_generator(405, "not_this_way")


@csrf_exempt
@api_view(['POST'])
def serach(request):
    if request.method != 'POST':
        return res_generator(405, "methode_not_allowed")

    api_token = request.POST['api_token']
    search_text = request.POST['search_text']
    if not isApiTokenValid(str(api_token)):
        return res_generator(401, "api_token")
    try:
        res = []

        coffee_shops_search_result = searchInCoffeeShops(str(search_text))
        for shop in coffee_shops_search_result:
            res.append(dict(lat=str(shop.lat),
                            lng=str(shop.lng),
                            name=str(shop.name),
                            address="",
                            type="coffee"))

        search_result = OpenStreetMapSearch(text_to_search=str(search_text))
        for item in search_result:
            if not str(item['langaddress']).__contains__("ایران") and not str(item['langaddress']).__contains__("iran"):
                continue
            res.append(dict(lat=item['lat'],
                            lng=item['lon'],
                            name=item['placename'],
                            address=item['langaddress'],
                            type="place"))

        return Response({"status": 200, "data": {"message": "ok", "results": res}})
    except Exception as e:
        return error_generator(500, "exception_in_search")


@csrf_exempt
@api_view(['POST'])
def getNearbyCoffees(request):
    if request.method != 'POST':
        return res_generator(405, "methode_not_allowed")

    api_token = request.POST['api_token']
    lat = request.POST['lat']
    lng = request.POST['lng']

    if not isApiTokenValid(str(api_token)):
        return res_generator(401, "api_token")
    try:
        res = []
        shops = searchNearbyCoffeeShops(float(lat), float(lng))
        for shop in shops:
            res.append(dict(shop_id=int(shop.shop_id),
                            lat=str(shop.lat),
                            lng=str(shop.lng),
                            name=str(shop.name)))

        return Response({"status": 200, "data": {"message": "ok", "results": res}})
    except Exception as e:
        return res_generator(500, "exception_in_search")


@csrf_exempt
@api_view(['POST'])
def getShopDetails(request):
    if request.method != 'POST':
        return res_generator(405, "methode_not_allowed")

    api_token = request.POST['api_token']
    shop_id = request.POST['shop_id']

    if not isApiTokenValid(str(api_token)):
        return res_generator(401, "api_token")
    try:
        shops = CoffeeShops.objects.filter(shop_id=shop_id).order_by('-shop_id')
        if shops.__len__() == 0:
            return res_generator(404, "shop_not_found")
        shop = shops[0]
        details = str(shop.description)
        work_time = getShopWorkTime(shop_id)
        mainPic = getMainPictureOfShop(shop_id)
        pictures = getPicturesOfShop(shop_id)
        menu = getShopMenu(int(shop.shop_id))

        return Response({"status": 200, "data": {"message": "ok",
                                                 "main_picture": mainPic,
                                                 "details": details,
                                                 "work_time": work_time,
                                                 "pictures": pictures,
                                                 "menu": menu}})
    except Exception as e:
        return res_generator(500, "exception_in_load_data")


@csrf_exempt
@api_view(['POST'])
def getOrdersList(request):
    if request.method != 'POST':
        return res_generator(405, "methode_not_allowed")

    api_token = request.POST['api_token']
    orders = request.POST['orders']

    if not isApiTokenValid(str(api_token)):
        return res_generator(401, "api_token")
    try:

        res = []

        orders_list = getOrdersListFromJsonString(str(orders))
        for order in orders_list:

            orders = CoffeeShopsMenus.objects.filter(item_id=int(order)).order_by('-item_id')
            if orders.__len__() == 0:
                continue

            shops = CoffeeShops.objects.filter(shop_id=int(orders[0].shop_id)).order_by('-shop_id')
            if shops.__len__() == 0:
                continue

            res.append(dict(item_id=order,
                            name=str(orders[0].name),
                            picture=str(orders[0].picture_link),
                            price=int(orders[0].price),
                            shop_name=str(shops[0].name)))

        return Response({"status": 200, "data": {"message": "ok", "orders": res}})
    except Exception as e:
        return res_generator(500, str(e))


@csrf_exempt
@api_view(['POST'])
def getOrdersAmount(request):
    if request.method != 'POST':
        return res_generator(405, "methode_not_allowed")

    api_token = request.POST['api_token']
    orders = request.POST['orders']
    discount_code = request.POST['discount_code']

    if not isApiTokenValid(str(api_token)):
        return res_generator(401, "api_token")
    try:

        amountSum = 0

        res = []

        shop_id = -1

        orders_list = getOrdersListFromJsonString(str(orders))
        for order in orders_list:

            orders = CoffeeShopsMenus.objects.filter(item_id=int(order)).order_by('-item_id')
            if orders.__len__() == 0:
                continue

            shops = CoffeeShops.objects.filter(shop_id=int(orders[0].shop_id)).order_by('-shop_id')
            if shops.__len__() == 0:
                continue
            shop_id = int(shops[0].shop_id)

            amountSum += int(orders[0].price)

        if str(discount_code).strip() == "":
            return Response({"status": 200, "data": {"message": "ok",
                                                     "total_amounts": amountSum,
                                                     "discount_amount": 0,
                                                     "amount_for_pay": amountSum}})

        discounts = CoffeeShopsDiscounts.objects.filter(code=str(discount_code))
        if discounts.__len__() == 0:
            return Response({"status": 200, "data": {"message": "ok",
                                                     "total_amounts": amountSum,
                                                     "discount_amount": 0,
                                                     "amount_for_pay": amountSum}})

        discount = discounts[0]

        if int(discount.use_count) <= 0 or int(discount.precent) <= 0 or (int(discount.shop_id) != shop_id and shop_id != -1):
            return Response({"status": 200, "data": {"message": "ok",
                                                     "total_amounts": amountSum,
                                                     "discount_amount": 0,
                                                     "amount_for_pay": amountSum}})

        precent = int(discount.precent)
        discount_amount = amountSum * (precent / 100)
        amount_for_pay = amountSum - discount_amount
        return Response({"status": 200, "data": {"message": "ok",
                                                 "total_amounts": amountSum,
                                                 "discount_amount": discount_amount,
                                                 "amount_for_pay": amount_for_pay}})

    except Exception as e:
        return res_generator(500, str(e))


@csrf_exempt
@api_view(['POST'])
def payOrderList(request):
    if request.method != 'POST':
        return res_generator(405, "methode_not_allowed")

    api_token = request.POST['api_token']
    orders = request.POST['orders']
    discount_code = request.POST['discount_code']

    if not isApiTokenValid(str(api_token)):
        return res_generator(401, "api_token")
    try:

        amountSum = 0

        res = []

        shop_id = -1
        shop_name = ""

        orders_list = getOrdersListFromJsonString(str(orders))
        for order in orders_list:

            dbOrders = CoffeeShopsMenus.objects.filter(item_id=int(order)).order_by('-item_id')
            if dbOrders.__len__() == 0:
                continue

            shops = CoffeeShops.objects.filter(shop_id=int(dbOrders[0].shop_id)).order_by('-shop_id')
            if shops.__len__() == 0:
                continue
            shop_id = int(shops[0].shop_id)
            shop_name = str(shops[0].name)

            amountSum += int(dbOrders[0].price)

        if str(discount_code).strip() == "":

            orderToSaveInDb = OrdersToPay()
            orderToSaveInDb.price = int(amountSum)
            orderToSaveInDb.shop_id = int(shop_id)
            orderToSaveInDb.user_id = getUserIdFromApiToken(str(api_token))
            orderToSaveInDb.orders_list = str(orders)
            orderToSaveInDb.use_discount = False
            orderToSaveInDb.save()
            order_id = int(orderToSaveInDb.order_id)

            description = "خرید از کافه " + shop_name
            client = zarinpalClient('https://www.zarinpal.com/pg/services/WebGate/wsdl')
            result = client.service.PaymentRequest(caffeMerchantCode, int(amountSum), description, "", "",
                                                   caffePaymentCallbackURL + str(order_id) + "/no")
            if result.Status == 100:
                result_url = str('https://www.zarinpal.com/pg/StartPay/' + str(result.Authority))
                return Response({"status": 200, "data": {"message": "ok",
                                                         "url": result_url}})
            else:
                return res_generator(500, "exception_in_load_data")

        discounts = CoffeeShopsDiscounts.objects.filter(code=str(discount_code))
        if discounts.__len__() == 0:

            orderToSaveInDb = OrdersToPay()
            orderToSaveInDb.price = int(amountSum)
            orderToSaveInDb.shop_id = int(shop_id)
            orderToSaveInDb.user_id = getUserIdFromApiToken(str(api_token))
            orderToSaveInDb.orders_list = str(orders)
            orderToSaveInDb.use_discount = False
            orderToSaveInDb.save()
            order_id = int(orderToSaveInDb.order_id)

            description = "خرید از کافه " + shop_name
            client = zarinpalClient('https://www.zarinpal.com/pg/services/WebGate/wsdl')
            result = client.service.PaymentRequest(caffeMerchantCode, int(amountSum), description, "", "",
                                                   caffePaymentCallbackURL + str(order_id) + "/no")
            if result.Status == 100:
                result_url = str('https://www.zarinpal.com/pg/StartPay/' + str(result.Authority))
                return Response({"status": 200, "data": {"message": "ok",
                                                         "url": result_url}})
            else:
                return res_generator(500, "exception_in_load_data")

        discount = discounts[0]

        if int(discount.use_count) <= 0 or int(discount.precent) <= 0 or (int(discount.shop_id) != shop_id and shop_id != -1):

            orderToSaveInDb = OrdersToPay()
            orderToSaveInDb.price = int(amountSum)
            orderToSaveInDb.shop_id = int(shop_id)
            orderToSaveInDb.user_id = getUserIdFromApiToken(str(api_token))
            orderToSaveInDb.orders_list = str(orders)
            orderToSaveInDb.use_discount = False
            orderToSaveInDb.save()
            order_id = int(orderToSaveInDb.order_id)

            description = "خرید از کافه " + shop_name
            client = zarinpalClient('https://www.zarinpal.com/pg/services/WebGate/wsdl')
            result = client.service.PaymentRequest(caffeMerchantCode, int(amountSum), description, "", "",
                                                   caffePaymentCallbackURL + str(order_id) + "/no")
            if result.Status == 100:
                result_url = str('https://www.zarinpal.com/pg/StartPay/' + str(result.Authority))
                return Response({"status": 200, "data": {"message": "ok",
                                                         "url": result_url}})
            else:
                return res_generator(500, "exception_in_load_data")

        precent = int(discount.precent)
        discount_amount = amountSum * (precent / 100)
        amount_for_pay = amountSum - discount_amount

        if precent == 100:
            orderToSaveInDb = OrdersToPay()
            orderToSaveInDb.price = int(amountSum)
            orderToSaveInDb.shop_id = int(shop_id)
            orderToSaveInDb.orders_list = str(orders)
            orderToSaveInDb.user_id = getUserIdFromApiToken(str(api_token))
            orderToSaveInDb.use_discount = True
            orderToSaveInDb.discount_code = str(discount_code)
            orderToSaveInDb.save()
            order_id = int(orderToSaveInDb.order_id)

            payment = OrdersPayments()
            payment.user_id = int(getUserIdFromApiToken(str(api_token)))
            payment.order_id = int(order_id)
            payment.discount_precent = 100
            payment.shop_id = int(shop_id)
            payment.amount = int(amountSum)
            payment.date = getNowDate()
            payment.with_discount = 1
            payment.status = 1
            payment.time = getNowTime()
            payment.description = "خرید از کافه " + shop_name + " با تخفیف ۱۰۰ درصد"
            payment.save()
            return Response({"status": 200, "data": {"message": "paid"}})

        orderToSaveInDb = OrdersToPay()
        orderToSaveInDb.price = int(amountSum)
        orderToSaveInDb.shop_id = int(shop_id)
        orderToSaveInDb.orders_list = str(orders)
        orderToSaveInDb.user_id = getUserIdFromApiToken(str(api_token))
        orderToSaveInDb.use_discount = True
        orderToSaveInDb.discount_code = str(discount_code)
        orderToSaveInDb.save()
        order_id = int(orderToSaveInDb.order_id)

        description = "خرید از کافه " + shop_name + " با تخفیف " + str(precent) + "درصد"
        client = zarinpalClient('https://www.zarinpal.com/pg/services/WebGate/wsdl')
        result = client.service.PaymentRequest(caffeMerchantCode, int(amount_for_pay), description, "", "",
                                               caffePaymentCallbackURL + str(order_id) + "/" + str(discount_code))
        if result.Status == 100:
            result_url = str('https://www.zarinpal.com/pg/StartPay/' + str(result.Authority))
            return Response({"status": 200, "data": {"message": "ok",
                                                     "url": result_url}})
        else:
            return res_generator(500, "exception_in_load_data")

    except Exception as e:
        return res_generator(500, "exception_in_load_data")


def resultOrderPayment(request, order_id, discount_code):
    backToAppLink = teamPaymentBackToAppBaseLink + str(getPostIdFromTeamId(int(team_id)))
    if request.GET.get('Status') == 'OK':
        client = zarinpalClient('https://www.zarinpal.com/pg/services/WebGate/wsdl')
        order = OrdersToPay.objects.filter(order_id=int(order_id))[0]
        order = OrdersToPay.objects.filter(order_id=int(order_id)).update(paid=True)
        price = int(order.price)
        dPrecent = 0
        if discount_code == "no":
            dPrecent = getDiscountPrecentFromCode(str(discount_code))

        result = client.service.PaymentVerification(caffeMerchantCode, request.GET['Authority'], price)
        payment = OrdersPayments()
        payment.shop_id = int(order.shop_id)
        date = getNowDate()
        time = getNowTime()
        payment.date = date
        payment.time = time
        payment.amount = int(price)
        if discount_code == "no":
            payment.description = "پرداخت بابت خرید از کافه " + getShopNameFromShopId(int(order.shop_id)) + " در ساعت " + str(time) + " در تاریخ " + str(date)
        else:
            payment.description = "پرداخت بابت خرید از کافه " + getShopNameFromShopId(int(order.shop_id)) + " در ساعت " + str(time) + " در تاریخ " + str(date) + " با تخفیف " + str(dPrecent) + " درصد"
            payment.with_discount = 1
            payment.discount_precent = dPrecent
        try:
            payment.user_id = str(order.user_id)
        except:
            payment.user_id = -1
        if result.Status == 100:
            if int(price) != 0:
                payment.status = 1
                payment.save()
                if discount_code != "no":
                    decreaseDiscountUseCountFromCode(str(discount_code))
            # code = str(result.RefID)
            # ref = "کد رهگیری : "
            # code = ref + code
            # ctx = {'refCode': code, 'text':"پرداخت شما با موفقیت انجام شد" , 'link': backToAppLink}
            # return render(request, "pardakht.html", ctx)
            # return HttpResponse('Transaction success.\nRefID: ' + str(result.RefID))
        elif result.Status == 101:
            if int(price) != 0:
                payment.status = 1
                payment.save()
                if discount_code != "no":
                    decreaseDiscountUseCountFromCode(str(discount_code))

            # ctx = {'refCode': "فاقد کد رهگیری", 'text': "پرداخت شما با موفقیت ثبت شد", 'link': backToAppLink}
            # return render(request, "pardakht.html", ctx)
            # return HttpResponse('Transaction submitted : ' + str(result.Status))
        else:
            if int(price) != 0:
                payment.status = 0
                payment.save()
            # ctx = {'refCode': "فاقد کد رهگیری", 'text': "پرداخت انجام نشد", 'link': backToAppLink}
            # return render(request, "pardakht.html", ctx)
            # return HttpResponse('Transaction failed.\nStatus: ' + str(result.Status))
    else:
        pass
        # ctx = {'refCode': "فاقد کد رهگیری", 'text': "پرداخت لغو شد", 'link': backToAppLink}
        # return render(request, "pardakht.html", ctx)


@csrf_exempt
@api_view(['POST'])
def getUserOrders(request):
    if request.method != 'POST':
        return res_generator(405, "methode_not_allowed")

    api_token = request.POST['api_token']

    if not isApiTokenValid(str(api_token)):
        return res_generator(401, "api_token")

    try:

        res = []

        user_id = getUserIdFromApiToken(str(api_token))
        orders = OrdersToPay.objects.filter(user_id=user_id, paid=True).order_by('-order_id')
        for order in orders:

            if not bool(order.use_discount):
                price = int(order.price)
            else:
                precent = getDiscountPrecentFromCode(str(order.discount_code))
                amount = int(order.price)
                price = int(amount - int(amount*(precent/100)))
            date, time = getOrderDateAndTimeWithOrderId(int(order.order_id))
            res.append(dict(order_id=int(order.order_id),
                            price=price,
                            orders_count=int(getOrdersCountFromOrdersList(str(order.orders_list))),
                            shop_name=str(getShopNameFromShopId(int(order.shop_id))),
                            shop_id=int(order.shop_id),
                            date=str(date),
                            time=str(time)))

        return Response({"status": 200, "data": {"message": "ok",
                                                 "orders": res}})
    except Exception as e:
        return res_generator(500, "exception_in_load_data")


@csrf_exempt
@api_view(['POST'])
def getMessages(request):
    if request.method != 'POST':
        return res_generator(405, "methode_not_allowed")

    api_token = request.POST['api_token']

    if not isApiTokenValid(str(api_token)):
        return res_generator(401, "api_token")

    try:

        result = []

        messages = Messages.objects.filter(Q(user_id=-1) | Q(user_id=getUserIdFromApiToken(str(api_token))))
        for message in messages:
            if not isMessagesDateOk(str(message.date)):
                continue
            if bool(message.has_link):
                temp_link = str(message.link)
            else:
                temp_link = ""
            result.append(dict(message_id=int(message.message_id),
                               text=str(message.text),
                               lable=str(message.lable),
                               date=str(message.date),
                               time=str(message.time),
                               link=str(temp_link)))
        return Response({"status": 200, "data": {"message": "ok",
                                                 "messages": result}})
    except Exception as e:
        return res_generator(500, "exception_in_load_data")


@csrf_exempt
@api_view(['POST'])
def editProfile(request):
    if request.method != 'POST':
        return res_generator(405, "methode_not_allowed")

    api_token = request.POST['api_token']
    username = request.POST['username']

    if not isApiTokenValid(str(api_token)):
        return res_generator(401, "api_token")

    try:
        users = Users.objects.filter(username=str(username))
        if users.__len__() != 0:
            return res_generator(409, "duplicate_username")
        Users.objects.filter(user_id=getUserIdFromApiToken(str(api_token))).update(username=str(username))
        return res_generator(200, "ok")
    except Exception as e:
        return res_generator(500, "exception_in_load_data")


@csrf_exempt
@api_view(['POST'])
def getProfile(request):
    if request.method != 'POST':
        return res_generator(405, "methode_not_allowed")

    api_token = request.POST['api_token']

    if not isApiTokenValid(str(api_token)):
        return res_generator(401, "api_token")

    try:
        users = Users.objects.filter(user_id=getUserIdFromApiToken(str(api_token)))
        if users.__len__() == 0:
            return res_generator(500, "exception_in_load_data")

        return Response({"status": 200, "data": {"message": "ok",
                                                 "username": str(users[0].username),
                                                 "phone_number": str(users[0].phone_number)}})
    except Exception as e:
        return res_generator(500, "exception_in_load_data")
