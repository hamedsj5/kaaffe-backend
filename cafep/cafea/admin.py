from django.contrib import admin
from .models import Tokens, Users, UsersTemp, CoffeeShops, CoffeeShopsMenus, CoffeeShopPictues
from .models import Messages, CoffeeShopsWrokTime, CoffeeShopsDiscounts, OrdersToPay, OrdersPayments

admin.site.register(Tokens)
admin.site.register(UsersTemp)
admin.site.register(Users)
admin.site.register(CoffeeShopsMenus)
admin.site.register(CoffeeShopPictues)
admin.site.register(CoffeeShops)
admin.site.register(CoffeeShopsWrokTime)
admin.site.register(CoffeeShopsDiscounts)
admin.site.register(OrdersPayments)
admin.site.register(OrdersToPay)
admin.site.register(Messages)
