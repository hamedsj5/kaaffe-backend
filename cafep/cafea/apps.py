from django.apps import AppConfig


class CafeaConfig(AppConfig):
    name = 'cafea'
