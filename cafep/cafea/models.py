from django.db import models


class Tokens(models.Model):
    token_id = models.AutoField(primary_key=True)
    user_id = models.IntegerField()
    api_token = models.CharField(max_length=100)
    agent = models.TextField()
    fcm_token = models.TextField()

    def __str__(self):
        return "user_id: "+str(self.user_id)+"\t|\tapi_token: "+str(self.api_token)

    class Meta:
        db_table = 'tokens'
        verbose_name_plural = "tokens"


class Users(models.Model):
    user_id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=50)
    pass_field = models.CharField(max_length=100, default="", db_column='pass')
    phone_number = models.CharField(max_length=20,default="")
    email = models.CharField(max_length=100, default="")
    with_google = models.PositiveSmallIntegerField(default=0)
    join_date = models.CharField(max_length=20)

    def __str__(self):
        return self.username + "\t|\tuser_id: "+str(self.user_id)

    class Meta:
        db_table = 'users'
        verbose_name_plural = "users"


class UsersTemp(models.Model):
    user_id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=50)
    pass_field = models.CharField(max_length=100, db_column='pass')
    phone_number = models.CharField(max_length=20)
    confirm_code = models.IntegerField()
    agent = models.TextField()
    fcm_token = models.TextField()

    def __str__(self):
        return self.username + "\t|\tuser_id: "+str(self.user_id)

    class Meta:
        db_table = 'users_temp'
        verbose_name_plural = "users_temp"


class CoffeeShops(models.Model):
    shop_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    lat = models.CharField(max_length=20)
    lng = models.CharField(max_length=20)
    description = models.CharField(max_length=200)

    def __str__(self):
        return self.name + "\t|\tshop_id: "+str(self.shop_id)

    class Meta:
        db_table = 'coffee_shops'
        verbose_name_plural = "coffee_shops"


class CoffeeShopsMenus(models.Model):
    item_id = models.AutoField(primary_key=True)
    shop_id = models.IntegerField()
    name = models.CharField(max_length=50)
    price = models.IntegerField()
    picture_link = models.TextField()
    item_type = models.CharField(max_length=20)
    exist = models.BooleanField()

    def __str__(self):
        return self.name + "\t|\titem_id: "+str(self.item_id)

    class Meta:
        db_table = 'coffee_shops_menus'
        verbose_name_plural = "coffee_shops_menus"


class CoffeeShopPictues(models.Model):
    picture_id = models.AutoField(primary_key=True)
    shop_id = models.IntegerField()
    link = models.TextField()
    isMain = models.BooleanField(default=False)

    def __str__(self):
        return self.link + "\t|\tpicture_id: "+str(self.picture_id)

    class Meta:
        db_table = 'coffee_shop_pictues'
        verbose_name_plural = "coffee_shop_pictues"


class CoffeeShopsWrokTime(models.Model):
    time_id = models.AutoField(primary_key=True)
    shop_id = models.IntegerField()
    start = models.CharField(max_length=20)
    end = models.CharField(max_length=20)

    def __str__(self):
        return "shop_id: " + str(self.shop_id) + "\t|\ttime_id: " + str(self.time_id)

    class Meta:
        db_table = 'coffee_shops_work_time'
        verbose_name_plural = "coffee_shops_work_time"


class CoffeeShopsDiscounts(models.Model):
    discount_id = models.AutoField(primary_key=True)
    code = models.CharField(max_length=50)
    precent = models.PositiveSmallIntegerField()
    shop_id = models.IntegerField()
    use_count = models.PositiveSmallIntegerField()

    def __str__(self):
        return str(self.code)

    class Meta:
        db_table = 'coffee_shops_discounts'
        verbose_name_plural = "coffee_shops_discounts"


class OrdersToPay(models.Model):
    order_id = models.AutoField(primary_key=True)
    user_id = models.IntegerField()
    shop_id = models.IntegerField()
    price = models.IntegerField()
    orders_list = models.TextField()
    use_discount = models.BooleanField()
    discount_code = models.CharField(max_length=50,default="no")
    paid = models.BooleanField(default=False)

    def __str__(self):
        return str(self.order_id)

    class Meta:
        db_table = 'orders_to_pay'
        verbose_name_plural = "orders_to_pay"


class OrdersPayments(models.Model):
    payment_id = models.AutoField(primary_key=True)
    user_id = models.IntegerField()
    shop_id = models.IntegerField()
    order_id = models.IntegerField()
    amount = models.IntegerField()
    description = models.TextField(default="")
    date = models.TextField()
    time = models.TextField()
    status = models.PositiveSmallIntegerField()
    with_discount = models.PositiveSmallIntegerField(default=0)
    discount_precent = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return str(self.payment_id)

    class Meta:
        db_table = 'orders_payments'
        verbose_name_plural = "orders_payments"


class Messages(models.Model):
    message_id = models.AutoField(primary_key=True)
    user_id = models.IntegerField()
    has_link = models.BooleanField(default=False)
    link = models.TextField(default="")
    text = models.TextField()
    lable = models.TextField(default="")
    date = models.TextField()
    time = models.TextField()

    def __str__(self):
        return str(self.text) + " | message_id : " + str(self.message_id)

    class Meta:
        db_table = 'messages'
        verbose_name_plural = "messages"
