from django.urls import path
from . import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
                  path('signup', views.signup, name='signup'),
                  path('login', views.login, name='login'),
                  path('confirmCode', views.confirmCode, name='confirmCode'),
                  path('forgetPass', views.forgetPass, name='forgetPass'),
                  path('loginWithGoogle', views.loginWithGoogle, name='loginWithGoogle'),
                  path('search', views.serach, name='search'),
                  path('getNearbyCoffees', views.getNearbyCoffees, name='getNearbyCoffees'),
                  path('getShopDetails', views.getShopDetails, name='getShopDetails'),
                  path('getOrdersList', views.getOrdersList, name='getOrdersList'),
                  path('getOrdersAmount', views.getOrdersAmount, name='getOrdersAmount'),
                  path('payOrderList', views.payOrderList, name='payOrderList'),
                  path('getUserOrders', views.getUserOrders, name='getUserOrders'),
                  path('getMessages', views.getMessages, name='getMessages'),
                  path('editProfile', views.editProfile, name='editProfile'),
                  path('getProfile', views.getProfile, name='getProfile'),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
