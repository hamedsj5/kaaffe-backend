import json
import requests

find1 = """<script type="text/javascript">
    var nominatim_map_init = {
    "zoom": 2,
    "lat": 20,
    "lon": 0,
    "tile_url": "https:\/\/{s}.tile.openstreetmap.org\/{z}\/{x}\/{y}.png",
    "tile_attribution": ""
};"""
find2 = "</script>"
additionalText1 = "var nominatim_results = "
additionalText2 = ";"


def OpenStreetMapSearch(text_to_search: str):
    try:
        baseUrl = "https://nominatim.openstreetmap.org/search.php?q="
        url = baseUrl + text_to_search
        r = requests.get(url=url)
        response = r.text
        indexFind1 = response.find(find1) + find1.__len__()
        slice1 = response[indexFind1:]
        indexFind2 = slice1.find(find2)
        slice2 = slice1[:indexFind2]
        result = slice2.replace(additionalText1, "").replace(additionalText2, "")
        json_result = json.loads(result)
        return json_result
    except Exception as e:
        return json.loads("[]")
